
//var winston = require('winston');
const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;

require('winston-daily-rotate-file');

// date_logger is used to log valid data in the payload of a request.
const date_logger = createLogger({
  level: 'info',
  transports: [
    new transports.Console({
      format: format.simple(),
      level: 'info'
    }),
    new transports.DailyRotateFile({
      // TBD: set correct filepath so that fluentd can read it.
      filename: './log/cai-date-log-%DATE%.log',
      datePattern: 'YYYY-MM-DD', // 'YYYY-MM-DD-HH-mm'
      prepend: true,
      level: 'info'
    })
  ],
  exitOnError: false,
});

// server_logger is used to keep error/warning records detected by this server.
const server_logger = createLogger({
  level: 'info',
  transports: [
    new transports.Console({
      format: format.simple(),
      level: 'info'
    }),
    new transports.DailyRotateFile({
      filename: './log/cai-server-log-%DATE%.log',
      datePattern: 'YYYY-MM', // 'YYYY-MM-DD-HH-mm'
      format: combine(timestamp(), format.simple()),
      prepend: true,
      level: 'info'
    })
  ],
  exitOnError: false
});

var express = require('express');
var bodyParser = require('body-parser')
var app = express();

/* for testing */
/*app.get('/log', function (req, res) {
  res.send('Hello World(GET)!\n');
});*/

app.post('/log', function validate_http_headers(req, res, next) {
  var content_type = req.headers['content-type'];
  if (!content_type || content_type.indexOf('application/json') == -1) {
    server_logger.warn('cai log server accepts json payload only!');
    return res.sendStatus(400);
  }

  next();
});

app.post('/log', bodyParser.json());

app.post('/log', function validate(req, res, next) {
  if (!req.body) {
    return res.sendStatus(400);
  }

  next();
});

app.post('/log', function filter(req, res, next) {
  var token = req.body['token'];
  if (token == undefined) {
    server_logger.warn('token is not pass.');
    return res.status(400).send('token is not pass.');
  }

  // A record is useless if any of the following id does not exist.
  var user_id = req.body['user_id'];
  var ua_id = req.body['ua_id'];
  var session_id = req.body['s_id'];

  if (user_id === undefined && ua_id === undefined && session_id === undefined) {
    server_logger.warn('id is not pass.');
    return res.status(400).send('id is not pass.');
  }
  next();
});

app.post('/log', function log_to_date_file(req, res, next) {
  var body = req.body;

  // TBD: why we need this log?
  var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  body['client_ip'] = ip;
  body['code_name'] = req['code_name'];
  if (req.headers['user-agent'] != undefined) {
    body['agent'] = req.headers['user-agent'];
  }
  // TBD: Really.... why we need this one? Is there a chance that we use GET
  // to log a tag? Even though, why should we care about it?
  body['request_method'] = req.method;
  body['api_logtime'] = Date.now();

  date_logger.info(JSON.stringify(body));
  next();
});
  
app.post('/log', function (req, res) {
  res.status(200).send('OK');
});

/*
Map<String, String> parameters = new HashMap<String, String>();
//If pt is "a", note as https connection.
if (origin_host.equals("n")) {
  if (id.equals("n")) {
    return "";
  }
  origin_host = ((pt.equals("a")) ? "https://" : "http://") + id;
}

if (client_ip.equals("n")) {
  client_ip = request.getRemoteAddr();
}

try {
  // get this API server's host name
  if (hostname == null) {
    hostname = InetAddress.getLocalHost().getHostName();
  }

} catch (UnknownHostException e) {
  VEN_LOGGER.warn(e.toString());
}

if (venguid.equals("n") && typ.equals("g")) {
  // The request does not has a guid in client side, gen. guid for it
  venguid = String.format(
          "%s.%s%s",
          UUID.randomUUID().toString(),
          hostname,
          ZonedDateTime.now().format(DateTimeFormatter.BASIC_ISO_DATE).replace("+0800", "")
  );

  Cookie guidCookie = new Cookie("venguid", venguid);
  guidCookie.setMaxAge(315360000); // 315360000 = 10year
  guidCookie.setDomain("venraas.tw");
  guidCookie.setPath("/");
  response.addCookie(guidCookie);
  parameters.put("ven_guid", venguid);
}

if (parameters.size() > 0) {
  //Log every gen. guid information if it happen
  parameters.put("agent", agent);
  parameters.put("client_ip", client_ip);
  parameters.put("origin", origin_host);
  parameters.put(
          "api_logtime",
          ZonedDateTime.now().format(
                  DateTimeFormatter.ISO_ZONED_DATE_TIME));
  guidlog.info(gson.toJson(parameters));
}

If type is "g", return guid string;
 If type is "s", return session string;
 Else return a space.

String rtn = " ";
switch (typ) {
  case "g":
    rtn = venguid;
    break;
  case "s":
    rtn = String.format(
            "%s.%s%s.se",
            UUID.randomUUID().toString(),
            hostname,
            ZonedDateTime.now().format(DateTimeFormatter.BASIC_ISO_DATE).replace("+0800", "")
    );
    break;
  default:
    break;
}

if (cbk.equals("y") && request.getMethod().equals("GET")) {
  //HTTP(S) jsonp GET protocol, this do for some browser which can't support HTTP(S) CORS POST protocol
  rtn = "vengujsonpcallbk('" + typ + "','" + rtn + "')";
} else {
  //HTTP(S) CORS POST protocol
  response.setHeader("Access-Control-Allow-Origin", origin_host);
  response.setHeader("Access-Control-Allow-Credentials", "true");
  response.setHeader("Access-Control-Allow-Methods", "GET,POST,OPTIONS");
}
*/

const uuidv4 = require('uuid/v4');
var cookieParser = require('cookie-parser');
app.use('/cai_ua_id', cookieParser());

// cai_ua_id service
app.use('/cai_ua_id', function (req, res, next) {
  if (req.cookies['cai_ua_id']) {
    return res.status(200).send();
  }

  var origin = req.headers['origin'] === undefined
               ? "changing.ai"
               : req.headers['origin'];

  //response.setHeader("Access-Control-Allow-Origin", origin_host);
  // TBD: combine time(uuidv1) and hostname
  const ua_id = uuidv4();
  res.cookie('cai_ua_id', ua_id, { maxAge: 315360000 /* 10 years */,
                                   domain:'.changing.ai',
                                   path: '/', })
     .set("Access-Control-Allow-Credentials", "true")
     .set("Access-Control-Allow-Methods", "GET,POST,OPTIONS")
     .set("Access-Control-Allow-Origin", origin)
     .contentType("text/plain").status(200).send(ua_id);
});

// cai_session_id service.
app.use('/cai_session_id', cookieParser());
app.use('/cai_session_id', function (req, res) {
  if (req.cookies['cai_session_id']) {
    return res.status(200).send();
  }

  var origin = req.headers['origin'] === undefined
               ? "changing.ai"
               : req.headers['origin'];

  // TBD: combine time(uuidv1) and hostname
  const session_id = uuidv4();
  res.contentType("text/plain").status(200)
     .set("Access-Control-Allow-Credentials", "true")
     .set("Access-Control-Allow-Methods", "GET,POST,OPTIONS")
     .set("Access-Control-Allow-Origin", origin)
     .send(session_id);
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

module.exports = app;
