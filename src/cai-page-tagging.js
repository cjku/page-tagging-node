
// TODO JSONP
const API_HOST_NAME = 'http://example.net'
const UA_ID_SERVICE = '/cai_ua_id'
const SESSION_ID_SERVICE = '/cai_session_id'
const LOG_SERVICE = '/log'

cai_utils = {
  xhr: function (method, url, headers) {
    var xhr = (
      window.XMLHttpRequest &&
      (window.location.protocol !== "file:" || !window.ActiveXObject)
    ) ? new window.XMLHttpRequest() : new window.ActiveXObject("Microsoft.XMLHTTP");

    xhr.open(method, url, true);
    for (var i in headers) {
      xhr.setRequestHeader(i, headers[i]);
    }
    xhr.withCredentials = true;

    return xhr;
  },
  is_mobile: function () {
    if (this.is_mobile !== undefined) {
      return this.is_mobile;
    }

    this.is_mobile = navigator.userAgent.indexOf('mobi') == -1 ? false : true;
    return this.is_mobile;
  }
};

cai_guid = {
  ua_id_max_age: 315360000000,
  session_id_max_age: 1800000,
  have_ua_id: function () { return this.ua_id !== undefined; },
  have_session_id: function () { return this.session_id !== undefined; },
  request_ua_id: function () {
    if (this.have_ua_id()) {
      return;
    }

    return this._request_guid('g', (guid) => {
      this._setup_cookie("ca_ua_id", guid, this.ua_id_max_age);
      this.ua_id = guid;
    });
  },
  request_session_id: function () {
    if (this.have_session_id()) {
      return;
    }

    return this._request_guid('s', (guid) => {
      this._setup_cookie("cai_seesion_id", guid, this.session_id_max_age);
      this.session_id = guid;
    });
  },
  refresh_session_id: function () {
    console.assert(this.have_session_id(), 'This function should be used after we fetch session_id');
    this._setup_cookie("cai_seesion_id", this.session_id, this.session_id_max_age);
  },
  empty: function () {
    delete this.session_id;
    delete this.ua_id;
  },
  _request_guid: function (type, callback) {
    // type: g << ua_id, s << session. TBC.
    // pt: a << https. TBC
    var params = "?id=" + top.location.host + "&typ=" + type + '&pt=a';
    const service_path = (type == 's') ? SESSION_ID_SERVICE : UA_ID_SERVICE;
    return new Promise((resolve, reject) => {
      var xhr = cai_utils.xhr("GET", API_HOST_NAME + service_path,
                              { "Content-type": "application/x-www-form-urlencoded;charset=UTF-8" });

      xhr.onreadystatechange = () => {
        if (xhr.readyState === XMLHttpRequest.DONE) {
          if (xhr.status === 200) {
            callback(xhr.responseText);
            resolve();
          } else {
            reject();
          }
        }
      };
      xhr.send(params);
    });
  },
  _setup_cookie: function (name, value, expirestime) {
    try {
      if (expirestime == 0) {
        document.cookie = name + "=" + escape(value) + ";Path=/" +
          ((cai_setting.domain_name !== undefined) ? ";domain=" + cai_setting.domain_name : '');
      } else {
        var expires = new Date();
        // N days,  N*24*60*60*1000.
        expires.setTime(expires.getTime() + expirestime);
        document.cookie = name + "=" + escape(value) + ";expires=" + expires.toGMTString() +
          ";Path=/" +
          ((cai_setting.domain_name !== undefined) ? ";domain=" + cai_setting.domain_name : '');
      }
    } catch (e) { }
  },
};

function cai_record(event_name, parameters) {
  this.event_name = event_name;
  this.parameters = {};
  this.add_data(parameters);
}

cai_record.prototype.add_data = function (parameters) {
  // deep copy
  for (var i in parameters) {
    if (Array.isArray(this.parameters[i])) {
      this.parameters[i] = this.parameters[i].concat(parameters[i]);
    } else {
      this.parameters[i] = parameters[i];
    }
  }
}

// IE 8 is not support Date().now()
if (!Date.now) {
  Date.now = function now() {
    return new Date().getTime();
  };
}

cai_record.prototype.pack_data = function (output) {
  this.parameters['ua_id'] = cai_guid.ua_id;
  this.parameters['s_id'] = cai_guid.session_id;

  // TBD: Figure out why we need all this information
  this.parameters['version'] = cai_setting.version;
  this.parameters['token'] = cai_setting.token;
  this.parameters['client_host'] = cai_setting.domain_setting;
  this.parameters['uri'] = location.pathname;
  this.parameters['tophost'] = top.location.host;

  this.parameters['referrer'] = document.referrer;

  // TBD: rename all the following items.
  this.parameters['para'] = location.search;

  try {
    this.parameters['client_tzo'] = (new Date()).getTimezoneOffset();
    this.parameters['client_utc'] = Date.now();
  } catch (e) { }

  if (this.parameters['from_rec'] === undefined) {
    var url = new URL(location.href);
    from_rec = url.searchParams.get('from_rec');
    if (from_rec) {
      this.parameters['from_rec'] = from_rec;
    }
  }

  if (this.parameters['device'] === undefined) {
    this.parameters['device'] = cai_utils.is_mobile() ? 'mb' : 'pc';
  }

  output.push(encodeURIComponent(this.event_name) + "=" + encodeURIComponent(JSON.stringify(this.parameters)));
}

var cai_record_set = {
  events: {},
  create: function (event_name, parameters) {
    this.events[event_name] = new cai_record(event_name, parameters);
    // deep copy.
    for (var i in parameters) {
      this.events[event_name].parameters[i] = parameters[i];
    }
    if (parameters['autosend'] === undefined || parameters['autosend'] == true) {
      this.send(event_name);
    }
  },
  add: function (event_name, parameters) {
    if (this.events[event_name] === undefined) {
      throw ('add_data should be called after create_data.');
    }
    this.events[event_name].add_data(parameters);
  },
  send: function (event_name) {
    var send_data = [];
    this.events[event_name].pack_data(send_data);
    send_data.join("&");
    this._send(send_data);
    delete this.events[event_name];
  },
  send_all: function () {
    var send_data = [];
    for (var i in this.events) {
      this.events[i].pack_data(send_data);
    }
    send_data.join("&");
    this._send(send_data);

    this.clear_all();
  },
  clear_all: function () {
    for (var i in this.events) {
      delete this.events[i];
    }
  },
  _send: function (output) {
    if (!cai_guid.have_session_id()) {
      throw 'session id must be ready.';
    }

    cai_guid.refresh_session_id();
    var xhr = cai_utils.xhr("POST", API_HOST_NAME + LOG_SERVICE, { "Content-type": "application/x-www-form-urlencoded;charset=UTF-8" });

    xhr.onreadystatechange = () => {
      try {

      } catch (e) {
        throw new Error("To be defined");
      }
    };

    xhr.send(output);
  },
};

var cai_setting = {
  'version': '0.0.1'
};

// We only support these kinds of commands:
// 1. 'create': create a tracking event.
//     For exp: cai('create' /* command */, 'pageLoad' /* tracking event */,
//                   { id: .... } /* event attributes */
// 2. 'add': add tracking attribute into a given tracking event.
//     For exp: cai('add' /* command */, 'pageLoad' /* tracking event */,
//                   { device: .... } /* event attributes */
// 3. 'init': init cai system.
//     For exp: cai('init' /* command */, { domain_name: xxx}  /* setting attriute */)
// 4. 'send': manually upload a tracking event.
//     For exp: cai('send' /* command */, 'cartLod' /* tracking */) /* update cartLoad */
//              cai('send' /* command */) /*update all tracking event */
// 5. 'clean': for testing only. Clean out everything we had done on global objects.
var command_dispatcher = {
  clean: function () {
    cai_guid.empty();
    cai_setting = new Object();
    cai_record_set.clear_all();
  },
  init: function (input_argument) {
    var setting = (input_argument.length > 0) ? input_argument[0] : undefined;
    if (!setting || !setting.token || setting.token.length == 0) {
      throw 'init must be called with token parameter';
    }

    if (cai_setting.token) {
      throw 'Double initiation';
    }

    // mandatory settings.
    cai_setting.token = setting.token;

    // optional setttings.
    if (setting.domain_name) {
      cai_setting.domain_name = setting.domain_name;
    }

    cai_guid.request_ua_id();
    cai_guid.request_session_id();

    if (document.getElementById('cai_frame') == null) {
      var cai_frame = document.createElement('iframe');
      cai_frame.setAttribute('id', 'cai_frame');
      cai_frame.style.display = "none";
      cai_frame.style.width = "1px";
      cai_frame.style.height = "1px";
      cai_frame.style.border = "0px";
      cai_frame.style.padding = "0px";
      cai_frame.style.margin = "0px";
      cai_frame.src = API_HOST_NAME + UA_ID_SERVICE;

      var host_element = cai_setting.host_element ? document.getElementById(cai_setting.host_element) : null;
      if (host_element == null) {
        document.body.appendChild(cai_frame);
      } else {
        host_element.appendChild(cai_frame);
      }
    }
  },
  create: function(input_argument) {
    var input = this._read_input(input_argument);

    this._dispatch(() => {
      if (cai_record_set.events[input.event_name] === undefined) {
        cai_record_set.create(input.event_name, input.parameters);
      }
    });
  },
  add: function(input_argument) {
    var input = this._read_input(input_argument);

    this._dispatch(() => {
      cai_record_set.add(input.event_name, input.parameters);
    });
  },
  send: function(input_argument) {
    var input = this._read_input(input_argument);

    this._dispatch(() => {
      if (input.event_name == undefined) {
        cai_record_set.send_all();
      } else {
        cai_record_set.send(input.event_name);
      }
    });
  },
  _read_input: function(input_argument) {
    if (!cai_setting.token) {
      throw 'cai(init) should be called beforehand';
    }

    var input = {};
    input.event_name = (input_argument.length > 0) ? input_argument[0] : undefined;
    input.parameters = (input_argument.length > 1) ? input_argument[1] : undefined;

    return input;
  },
  _dispatch: function(callback) {
    if (cai_guid.have_session_id()) {
      callback();
    } else {
      cai_guid.request_ua_id().then(() => { callback(); });
    }
  }
};

function cai(command) {
  if (command_dispatcher[command] === undefined) {
    throw new Error("wrong command");
  }

  var args = Array.prototype.slice.call(arguments, 1);
  command_dispatcher[command](args);
}
