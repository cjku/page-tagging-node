var chai = require('chai');
var chaiHttp = require('chai-http');
var should = require('chai').should();
var expect = require('chai').expect;
var assert = require('chai').assert;

let server = require('../src/log-service.js');

chai.use(chaiHttp);

describe('Guil/cookie Restful API test', function() {
  it('Should return cai_ua_id if cookie was not passed', function(done) {
    chai.request(server)
      .post('/cai_ua_id')
      .set('origin', 'https://example.com')
      .send()
      .end((err, res) => {
        res.should.have.status(200);
        res.should.have.cookie('cai_ua_id');
        res.should.be.text;
        res.text.should.have.lengthOf.above(5);
        done();
      });
  });

  it('Should not return cai_ua_id if cookie was passed', function(done) {
    chai.request(server)
      .post('/cai_ua_id')
      .set('Cookie', 'cai_ua_id=foo')
      .send()
      .end((err, res) => {
        res.should.have.status(200);
        res.should.not.have.cookie('cai_ua_id');
        done();
      });
  });

  it('Should return cai_session_id', function(done) {
    chai.request(server)
      .post('/cai_session_id')
      .set('origin', 'https://example.com')
      .send()
      .end((err, res) => {
        res.should.have.status(200);
        res.should.not.have.cookie('cai_session_id');
        res.should.be.text;
        res.text.should.have.lengthOf.above(5);
        done();
      });
  });
});

// TBD: we should define data schema bottom-up.
describe('WebLog Restful API test', function() {
  it('server should return error code if token is not pass', function(done) {
    chai.request(server)
      .post('/log')
      .set('content-type', 'application/json')
      .send({ foo: 'bar', boo: 'var', user_id: 'ted' })
      .end((err, res) => {
        res.should.have.status(400);
        done();
      });
  });

  //  HTTP request library under the hood of Chai-HTTP, which auto-detects
  //  the content-type based on what kind of data is contained in the .send() call.
  //  Setting content-type as text/html is not enough, we have to put html content
  //  as a parameter of send function.
  it('server should return error code if content-type is not json', function(done) {
    chai.request(server)
      .post('/log')
      .set('content-type', 'text/html')
      .send("<html></html>")
      .end((err, res) => {
        res.should.have.status(400);
        done();
      });
  });
 
  it('server should return success code if all mandatory information pass is pass(1).', function(done) {
    // token + user_id
    chai.request(server)
      .post('/log')
      .set('content-type', 'application/json')
      .send({ foo: 'bar', boo: 'var', token: 'cia0001', user_id: 'ted' })
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  });
  it('server should return success code if all mandatory information pass is pass(2).', function(done) {
    // token + ua id
    chai.request(server)
      .post('/log')
      .set('content-type', 'application/json')
      .send({ foo: 'bar', boo: 'var', token: 'cia0001', ua_id: 'Webkit(Mozilla)' })
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  });
  it('server should return success code if all mandatory information pass is pass(3).', function(done) {
    // token + session id
    chai.request(server)
      .post('/log')
      .set('content-type', 'application/json')
      .send({ foo: 'bar', boo: 'var', token: 'cia0001', s_id: '2017_01_02_started_session' })
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  });
});

